﻿Shader "Custom/RenderShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "unityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert(appdata v)
			{
				v2f ret;
				ret.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				ret.uv = v.uv;
				return ret;
			}

			sampler2D _MainTex;

			float4 frag(v2f input) : SV_Target
			{
#if SHADER_API_D3D9 || SHADER_API_D3D11
				input.uv.y = 1 - input.uv.y;
#endif
				float4 color = tex2D(_MainTex, input.uv);
				return color;
			}
			ENDCG
		}
	} 
}
