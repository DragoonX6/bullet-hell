﻿using UnityEngine;

public class FloorGenerator: MonoBehaviour
{
	public GameObject tilePrefab = null;
	public Camera viewPort = null;

	public float tileDistance = 0.0f;

	private void Awake()
	{
		if(tilePrefab == null)
		{
			Debug.LogError("No tile prefab set.");
			return;
		}

		if(viewPort == null)
		{
			Debug.LogError("No camera set.");
			return;
		}

		float cameraDistance = Vector3.Distance(viewPort.transform.position, Vector3.zero);

		Vector3 startPosX = viewPort.ScreenToWorldPoint(new Vector3(0, 0,
			viewPort.farClipPlane));
		Vector3 startPosZ = viewPort.ScreenToWorldPoint(new Vector3(0, 0, cameraDistance));
		Vector3 startPosXZ = new Vector3(startPosX.x, 0, startPosZ.z);
		Vector3 endPosX = viewPort.ScreenToWorldPoint(
			new Vector3(viewPort.pixelWidth, 0, viewPort.farClipPlane));
		Vector3 endPosZ = viewPort.ScreenToWorldPoint(
			new Vector3(0, viewPort.pixelHeight, viewPort.farClipPlane));

		MeshFilter tileMeshFilter = tilePrefab.GetComponent<MeshFilter>();
		Vector3 tileSize = tileMeshFilter.sharedMesh.bounds.size;

		int xTiles = (Mathf.CeilToInt(Vector3.Distance(startPosXZ, endPosX) /
			(tileSize.x + tileDistance))) + 2;
		int zTiles = (Mathf.CeilToInt(Vector3.Distance(startPosXZ, endPosZ) /
			(tileSize.y + tileDistance))) + 6;
		zTiles += zTiles % 2;
		xTiles += xTiles % 2;

		startPosXZ.x = Mathf.Floor(startPosXZ.x /*+ (tileSize.y + tileDistance)*/);
		startPosXZ.z = Mathf.Floor(startPosXZ.z -
			(tileSize.y + tileDistance - (tileSize.x - tileSize.y)));

		FloorTile[,] tileGrid = new FloorTile[zTiles, xTiles];

		for(int z = 0; z < zTiles; ++z)
		{
			for(int x = 0; x < xTiles; ++x)
			{
				Vector3 position;
				FloorTile child = null;
				if((z % 2) != 0)
				{
					float xPos = startPosXZ.x +
						-(tileSize.y * 0.5f + (tileDistance * 0.5f)) +
						(x * (tileSize.y + tileDistance));
					float zPos = startPosXZ.z - z *
						-(tileSize.y + tileDistance - (tileSize.x - tileSize.y));

                    position = new Vector3(xPos, 0, zPos);
                    
				}
				else
				{
					float xPos = startPosXZ.x + (x * (tileSize.y + tileDistance));
					float zPos = startPosXZ.z - z *
						-(tileSize.y + tileDistance - (tileSize.x - tileSize.y));

					position = new Vector3(xPos, 0, zPos);
				}

				child = (Instantiate(tilePrefab, position,
						tilePrefab.transform.rotation) as GameObject).
						GetComponent<FloorTile>();

				child.transform.SetParent(transform);
				child.name = string.Format("Floor_Tile {0};{1}", z, x);

				tileGrid[z, x] = child;
			}
		}

		FloorController floorController = GetComponent<FloorController>();
		floorController.Initialize(tileGrid);
	}
}
