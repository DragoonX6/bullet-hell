﻿using UnityEngine;
using System.Collections.Generic;

[RequireComponent(typeof(FloorGenerator))]
public class FloorController: MonoBehaviour
{
	[SerializeField]
	private FloorTile bottomLeft = null, topRight = null;

	private float tileDistance = 0.0f;

	private FloorTile topBoundaryTile = null,
		rightBoundaryTile = null;

	private void Awake()
	{
		tileDistance = GetComponent<FloorGenerator>().tileDistance;
	}

	// Z, X
	// good luck
	public void Initialize(FloorTile[,] tileGrid)
	{
		bottomLeft = tileGrid[0, 0];

		int zSize = tileGrid.GetLength(0);
		int xSize = tileGrid.GetLength(1);

		topRight = tileGrid[zSize - 1, xSize - 1];

		#region bottomRow
		tileGrid[0, 0].topLeftNeighbour = tileGrid[1, 0];
		for(int x = 1; x < xSize; ++x)
		{
			tileGrid[0, x].leftNeighbour = tileGrid[0, x - 1];
			tileGrid[0, x - 1].rightNeighbour = tileGrid[0, x];
			tileGrid[0, x - 1].topRightNeighbour = tileGrid[1, x];
			tileGrid[0, x - 1].topLeftNeighbour = tileGrid[1, x - 1];
		}
		//tileGrid[0, xSize - 2].topLeftNeighbour = tileGrid[1, xSize - 2];
		//tileGrid[0, xSize - 2].topRightNeighbour = tileGrid[1, xSize - 1];
		tileGrid[0, xSize - 1].topLeftNeighbour = tileGrid[1, xSize - 1];
		#endregion

		for(int z = 1; z < zSize - 1; ++z)
		{
			if(z % 2 != 0)
			{
				tileGrid[z, 0].topRightNeighbour = tileGrid[z + 1, 0];
				tileGrid[z, 0].bottomRightNeighbour = tileGrid[z - 1, 0];
				for(int x = 1; x < xSize; ++x)
				{
					tileGrid[z, x].leftNeighbour = tileGrid[z, x - 1];
					tileGrid[z, x - 1].rightNeighbour = tileGrid[z, x];
					tileGrid[z, x].topLeftNeighbour = tileGrid[z + 1, x - 1];
					tileGrid[z, x].topRightNeighbour = tileGrid[z + 1, x];
					tileGrid[z, x].bottomLeftNeighbour = tileGrid[z - 1, x - 1];
					tileGrid[z, x].bottomRightNeighbour = tileGrid[z - 1, x];
				}
				tileGrid[z, xSize - 1].leftNeighbour = tileGrid[z, xSize - 2];
				tileGrid[z, xSize - 2].rightNeighbour = tileGrid[z, xSize - 1];
				tileGrid[z, xSize - 1].topLeftNeighbour = tileGrid[z + 1, xSize - 2];
				tileGrid[z, xSize - 1].bottomLeftNeighbour = tileGrid[z - 1, xSize - 2];
			}
			else
			{
				tileGrid[z, 0].topLeftNeighbour = tileGrid[z + 1, 0];
				tileGrid[z, 0].topRightNeighbour = tileGrid[z + 1, 1];
				tileGrid[z, 0].bottomLeftNeighbour = tileGrid[z - 1, 0];
				tileGrid[z, 0].bottomRightNeighbour = tileGrid[z - 1, 1];
				for(int x = 1; x < xSize - 1; ++x)
				{
					tileGrid[z, x].leftNeighbour = tileGrid[z, x - 1];
					tileGrid[z, x - 1].rightNeighbour = tileGrid[z, x];
					tileGrid[z, x].topLeftNeighbour = tileGrid[z + 1, x];
					tileGrid[z, x].topRightNeighbour = tileGrid[z + 1, x + 1];
					tileGrid[z, x].bottomLeftNeighbour = tileGrid[z - 1, x];
					tileGrid[z, x].bottomRightNeighbour = tileGrid[z - 1, x + 1];
				}

				tileGrid[z, xSize - 1].leftNeighbour = tileGrid[z, xSize - 2];
				tileGrid[z, xSize - 2].rightNeighbour = tileGrid[z, xSize - 1];
				tileGrid[z, xSize - 1].topLeftNeighbour = tileGrid[z + 1, xSize - 1];
				tileGrid[z, xSize - 1].bottomLeftNeighbour = tileGrid[z - 1, xSize - 1];
			}
		}

		#region topRow
		for(int x = 1; x < xSize; ++x)
		{
			tileGrid[zSize - 1, x].leftNeighbour = tileGrid[zSize - 1, x - 1];
			tileGrid[zSize - 1, x - 1].rightNeighbour = tileGrid[zSize - 1, x];
			tileGrid[zSize - 1, x - 1].bottomRightNeighbour = tileGrid[zSize - 2, x - 1];
			if((x - 2) >= 0)
				tileGrid[zSize - 1, x - 1].bottomLeftNeighbour = tileGrid[zSize - 2, x - 2];
		}
		tileGrid[zSize - 1, xSize - 1].bottomLeftNeighbour = tileGrid[zSize - 2, xSize - 2];
		tileGrid[zSize - 1, xSize - 1].bottomRightNeighbour = tileGrid[zSize - 2, xSize - 1];
		#endregion
	}

	public void InitBoundaries(GameObject player)
	{
		List<FloorTile> tiles = GetTiles(player.transform.position);
		FloorTile tile = GetBestQualifyingTile(tiles, player.transform.position);

		Vector3 playerPosition = player.transform.position;
		playerPosition.x = tile.transform.position.x;
		playerPosition.z = tile.transform.position.z;
		player.transform.position = playerPosition;

		// Get the top and right tile 4 tiles away
		topBoundaryTile = tile.topRightNeighbour.topLeftNeighbour.
			topRightNeighbour.topLeftNeighbour;

		rightBoundaryTile = tile.rightNeighbour.rightNeighbour;
	}

	public List<FloorTile> GetTiles(Vector3 position)
	{
		List<FloorTile> foundTiles = new List<FloorTile>(3);

		FloorTile currentTile;

		#region bottomUpSearch
		if(Vector3.Distance(bottomLeft.transform.position, position) <=
			Vector3.Distance(topRight.transform.position, position))
		{
			currentTile = bottomLeft;

			do
			{
				float xStart = currentTile.transform.position.x - (FloorTile.Size.x * 0.5f);
				float xEnd = xStart + FloorTile.Size.x;

				if(position.x >= xStart && position.x <= xEnd)
					break;

				currentTile = position.x > currentTile.transform.position.x ?
					currentTile.rightNeighbour : currentTile.leftNeighbour;
			} while(currentTile);

			if(!currentTile)
				return foundTiles;

			bool goTopRight = currentTile.rightNeighbour != null;
			do
			{
				float yStart = currentTile.transform.position.z - (FloorTile.Size.x * 0.5f);
				float yEnd = yStart + FloorTile.Size.x;

				if(position.z >= yStart && position.z <= yEnd)
					break;

				currentTile = goTopRight ? currentTile.topRightNeighbour :
					currentTile.topLeftNeighbour;
				goTopRight = !goTopRight;
			} while(currentTile);

			if(!currentTile)
				return foundTiles;

			do
			{
				float xStart = currentTile.transform.position.x - (FloorTile.Size.x * 0.5f);
				float xEnd = xStart + FloorTile.Size.x;

				if(position.x >= xStart && position.x <= xEnd)
					break;

				currentTile = position.x > currentTile.transform.position.x ?
					currentTile.rightNeighbour : currentTile.leftNeighbour;
			} while(currentTile);

			if(!currentTile)
				return foundTiles;

			foundTiles.Add(currentTile);

			FloorTile topLeftTile = currentTile.topLeftNeighbour;
			if(topLeftTile)
			{
				float xStart = topLeftTile.transform.position.x - (FloorTile.Size.x * 0.5f);
				float xEnd = xStart + FloorTile.Size.x;
				float yStart = topLeftTile.transform.position.z - (FloorTile.Size.x * 0.5f);
				float yEnd = yStart + FloorTile.Size.x;

				if(position.z >= yStart && position.z <= yEnd &&
					position.x >= xStart && position.x <= xEnd)
				{
					foundTiles.Add(topLeftTile);
				}
			}

			FloorTile topRightTile = currentTile.topRightNeighbour;
			if(topRightTile)
			{
				float xStart = topRightTile.transform.position.x - (FloorTile.Size.x * 0.5f);
				float xEnd = xStart + FloorTile.Size.x;
				float yStart = topRightTile.transform.position.z - (FloorTile.Size.x * 0.5f);
				float yEnd = yStart + FloorTile.Size.x;

				if(position.z >= yStart && position.z <= yEnd &&
					position.x >= xStart && position.x <= xEnd)
				{
					foundTiles.Add(topRightTile);
				}
			}
		}
		#endregion
		#region topDownSearch
		else
		{
			currentTile = topRight;

			do
			{
				float xStart = currentTile.transform.position.x - (FloorTile.Size.x * 0.5f);
				float xEnd = xStart + FloorTile.Size.x;

				if(position.x >= xStart && position.x <= xEnd)
					break;

				currentTile = position.x > currentTile.transform.position.x ? currentTile.rightNeighbour : currentTile.leftNeighbour;
			} while(currentTile);

			if(!currentTile)
				return foundTiles;

			bool goBottomLeft = currentTile.bottomLeftNeighbour != null;
			do
			{
				float yStart = currentTile.transform.position.z - (FloorTile.Size.x * 0.5f);
				float yEnd = yStart + FloorTile.Size.x;

				if(position.z >= yStart && position.z <= yEnd)
					break;

				currentTile = goBottomLeft ? currentTile.bottomLeftNeighbour : currentTile.bottomRightNeighbour;
				goBottomLeft = !goBottomLeft;
			} while(currentTile);

			if(!currentTile)
				return foundTiles;

			do
			{
				float xStart = currentTile.transform.position.x - (FloorTile.Size.x * 0.5f);
				float xEnd = xStart + FloorTile.Size.x;

				if(position.x >= xStart && position.x <= xEnd)
					break;

				currentTile = position.x > currentTile.transform.position.x ? currentTile.rightNeighbour : currentTile.leftNeighbour;
			} while(currentTile);

			if(!currentTile)
				return foundTiles;

			foundTiles.Add(currentTile);

			FloorTile bottomLeftTile = currentTile.bottomLeftNeighbour;
			if(bottomLeftTile)
			{
				float xStart = bottomLeftTile.transform.position.x - (FloorTile.Size.x * 0.5f);
				float xEnd = xStart + FloorTile.Size.x;
				float yStart = bottomLeftTile.transform.position.z - (FloorTile.Size.x * 0.5f);
				float yEnd = yStart + FloorTile.Size.x;

				if(position.z >= yStart && position.z <= yEnd &&
					position.x >= xStart && position.x <= xEnd)
				{
					foundTiles.Add(bottomLeftTile);
				}
			}

			FloorTile bottomRightTile = currentTile.bottomRightNeighbour;
			if(bottomRightTile)
			{
				float xStart = bottomRightTile.transform.position.x - (FloorTile.Size.x * 0.5f);
				float xEnd = xStart + FloorTile.Size.x;
				float yStart = bottomRightTile.transform.position.z - (FloorTile.Size.x * 0.5f);
				float yEnd = yStart + FloorTile.Size.x;

				if(position.z >= yStart && position.z <= yEnd &&
					position.x >= xStart && position.x <= xEnd)
				{
					foundTiles.Add(bottomRightTile);
				}
			}
		}
		#endregion

		FloorTile leftTile = currentTile.leftNeighbour;
		if(leftTile)
		{
			float xStart = leftTile.transform.position.x - (FloorTile.Size.x * 0.5f);
			float xEnd = xStart + FloorTile.Size.x;
			float yStart = leftTile.transform.position.z - (FloorTile.Size.x * 0.5f);
			float yEnd = yStart + FloorTile.Size.x;

			if(position.z >= yStart && position.z <= yEnd &&
				position.x >= xStart && position.x <= xEnd)
			{
				foundTiles.Add(leftTile);
			}
		}

		FloorTile rightTile = currentTile.rightNeighbour;
		if(rightTile)
		{
			float xStart = rightTile.transform.position.x - (FloorTile.Size.x * 0.5f);
			float xEnd = xStart + FloorTile.Size.x;
			float yStart = rightTile.transform.position.z - (FloorTile.Size.x * 0.5f);
			float yEnd = yStart + FloorTile.Size.x;

			if(position.z >= yStart && position.z <= yEnd &&
				position.x >= xStart && position.x <= xEnd)
			{
				foundTiles.Add(rightTile);
			}
		}

		return foundTiles;
    }

	private FloorTile GetBestQualifyingTile(List<FloorTile> tiles, Vector3 position)
	{
		float distance = float.MaxValue;
		int index = -1;

		for(int i = 0; i < tiles.Count; ++i)
		{
			if(Vector3.Distance(tiles[i].transform.position, position) < distance)
				index = i;
		}

		return tiles[index];
	}

	public void OnPlayerMove(Vector3 newPosition)
	{
		List<FloorTile> tiles = GetTiles(newPosition);
		FloorTile tile = GetBestQualifyingTile(tiles, newPosition);

		FloorTile currentTile = tile;

		int leftMovement = -4;
		bool tileSwitch = true;

		do
		{
			float distance = currentTile.transform.position.x -
				rightBoundaryTile.transform.position.x;
            if(distance <= float.Epsilon && distance >= 0)
				break;

			if(currentTile.transform.position.x < rightBoundaryTile.transform.position.x)
			{
				currentTile = tileSwitch ? currentTile.topRightNeighbour :
					currentTile.bottomRightNeighbour;
				++leftMovement;
            }
			else
			{
				currentTile = tileSwitch ? currentTile.topLeftNeighbour :
					currentTile.bottomLeftNeighbour;
				--leftMovement;
			}

			tileSwitch = !tileSwitch;
		} while(currentTile);

		int bottomMovement = -4;
		currentTile = tile;

		do
		{
			float distance = currentTile.transform.position.z -
				topBoundaryTile.transform.position.z;
            if(distance <= float.Epsilon && distance >= 0)
				break;

			if(currentTile.transform.position.z < topBoundaryTile.transform.position.z)
			{
				currentTile = tileSwitch ? currentTile.topRightNeighbour :
					currentTile.topLeftNeighbour;

				++bottomMovement;
			}
			else
			{
				currentTile = tileSwitch ? currentTile.bottomRightNeighbour :
					currentTile.bottomLeftNeighbour;

				--bottomMovement;
			}
		} while(currentTile);

		while(leftMovement != 0)
		{
			if(leftMovement < 0)
			{
				MoveLeftRowToRight();
				++leftMovement;
			}
			else
			{
				MoveRightRowToLeft();
				--leftMovement;
			}
		}

		while(bottomMovement != 0)
		{
			if(bottomMovement < 0)
			{
				MoveBottomRowToTop();
				++bottomMovement;
			}
			else
			{
				MoveTopRowToBottom();
				--bottomMovement;
			}
		}

		// Get the top and right tile 4 tiles away
		topBoundaryTile = tile.topRightNeighbour.topLeftNeighbour.
			topRightNeighbour.topLeftNeighbour;

		rightBoundaryTile = tile.rightNeighbour.rightNeighbour;
	}

	// move leftmost row to rightmost side
	private void MoveLeftRowToRight()
	{
		List<FloorTile> leftRow = new List<FloorTile>(),
			rightRow = new List<FloorTile>();

		FloorTile currentTile = bottomLeft;

		do
		{
			if(!currentTile.topLeftNeighbour && !currentTile.bottomLeftNeighbour)
			{
				leftRow.Add(currentTile);
				currentTile = currentTile.topRightNeighbour;
			}
			else
				currentTile = currentTile.topLeftNeighbour;
		} while(currentTile);

		currentTile = topRight;

		do
		{
			if(currentTile.bottomRightNeighbour || currentTile.topRightNeighbour)
			{
				rightRow.Add(currentTile);
				currentTile = currentTile.bottomRightNeighbour;
			}
			else
				currentTile = currentTile.bottomLeftNeighbour;
		} while(currentTile);

		rightRow.Reverse();

		Debug.Assert(rightRow.Count == leftRow.Count);

		if(leftRow[0] == bottomLeft)
			bottomLeft = bottomLeft.rightNeighbour;

		for(int i = 0; i < leftRow.Count; ++i)
		{
			leftRow[i].transform.position = rightRow[i].transform.position +
				new Vector3(FloorTile.Size.y + tileDistance, 0);

			if(leftRow[i].bottomRightNeighbour)
			{
				leftRow[i].bottomRightNeighbour.topLeftNeighbour = null;
				leftRow[i].bottomRightNeighbour = null;
			}

			if(leftRow[i].topRightNeighbour)
			{
				leftRow[i].topRightNeighbour.bottomLeftNeighbour = null;
				leftRow[i].topRightNeighbour = null;
			}

			leftRow[i].rightNeighbour.leftNeighbour = null;
			leftRow[i].rightNeighbour = null;

			leftRow[i].leftNeighbour = rightRow[i];
			rightRow[i].rightNeighbour = leftRow[i];

			if(rightRow[i].topRightNeighbour)
			{
				leftRow[i].topLeftNeighbour = rightRow[i].topRightNeighbour;
				leftRow[i].topLeftNeighbour.bottomRightNeighbour = leftRow[i];
			}

			if(rightRow[i].bottomRightNeighbour)
			{
				leftRow[i].bottomLeftNeighbour = rightRow[i].bottomRightNeighbour;
				leftRow[i].bottomLeftNeighbour.topRightNeighbour = leftRow[i];
			}
		}

		if(topRight.rightNeighbour != null)
			topRight = topRight.rightNeighbour;
	}

	// move righttmost row to leftmost side
	private void MoveRightRowToLeft()
	{
		List<FloorTile> leftRow = new List<FloorTile>(),
			rightRow = new List<FloorTile>();

		FloorTile currentTile = bottomLeft;

		do
		{
			if(currentTile.topLeftNeighbour || currentTile.bottomLeftNeighbour)
			{
				leftRow.Add(currentTile);
				currentTile = currentTile.topLeftNeighbour;
			}
			else
				currentTile = currentTile.topRightNeighbour;
		} while(currentTile);

		currentTile = topRight;

		do
		{
			if(!currentTile.bottomRightNeighbour && !currentTile.topRightNeighbour)
			{
				rightRow.Add(currentTile);
				currentTile = currentTile.bottomLeftNeighbour;
			}
			else
				currentTile = currentTile.bottomRightNeighbour;
		} while(currentTile);

		if(rightRow[0] == topRight)
			topRight = topRight.leftNeighbour;

		rightRow.Reverse();

		Debug.Assert(rightRow.Count == leftRow.Count);

		for(int i = 0; i < leftRow.Count; ++i)
		{
			rightRow[i].transform.position = leftRow[i].transform.position +
				-new Vector3(FloorTile.Size.y + tileDistance, 0);

			if(rightRow[i].bottomLeftNeighbour)
			{
				rightRow[i].bottomLeftNeighbour.topRightNeighbour = null;
				rightRow[i].bottomLeftNeighbour = null;
			}

			if(rightRow[i].topLeftNeighbour)
			{
				rightRow[i].topLeftNeighbour.bottomRightNeighbour = null;
				rightRow[i].topLeftNeighbour = null;
			}

			rightRow[i].leftNeighbour.rightNeighbour = null;
			rightRow[i].leftNeighbour = null;

			rightRow[i].rightNeighbour = leftRow[i];
			leftRow[i].leftNeighbour = rightRow[i];

			if(leftRow[i].topLeftNeighbour)
			{
				rightRow[i].topRightNeighbour = leftRow[i].topLeftNeighbour;
				rightRow[i].topRightNeighbour.bottomLeftNeighbour = rightRow[i];
			}

			if(leftRow[i].bottomLeftNeighbour)
			{
				rightRow[i].bottomRightNeighbour = leftRow[i].bottomLeftNeighbour;
				rightRow[i].bottomRightNeighbour.topLeftNeighbour = rightRow[i];
			}
		}

		if(bottomLeft.leftNeighbour != null)
			bottomLeft = bottomLeft.leftNeighbour;
	}

	private void MoveBottomRowToTop()
	{
		List<FloorTile> bottomRow = new List<FloorTile>(),
			topRow = new List<FloorTile>();

		FloorTile currentTile = bottomLeft;

		do
		{
			bottomRow.Add(currentTile);
			currentTile = currentTile.rightNeighbour;
		} while(currentTile);

		currentTile = topRight;

		do
		{
			topRow.Add(currentTile);
			currentTile = currentTile.leftNeighbour;
		} while(currentTile);

		Debug.Assert(bottomRow.Count == topRow.Count);

		topRow.Reverse();

		bool isInnerRow = bottomLeft.topLeftNeighbour != null;

		bottomLeft = isInnerRow ? bottomLeft.topLeftNeighbour : bottomLeft.topRightNeighbour;

		for(int i = 0; i < bottomRow.Count; ++i)
		{
			Vector3 currentPos = bottomRow[i].transform.position;
			currentPos.z = topRow[i].transform.position.z +
				(FloorTile.Size.y + tileDistance - (FloorTile.Size.x - FloorTile.Size.y));
			bottomRow[i].transform.position = currentPos;

			if(bottomRow[i].topRightNeighbour)
				bottomRow[i].topRightNeighbour.bottomLeftNeighbour = null;

			bottomRow[i].topRightNeighbour = null;

			if(bottomRow[i].topLeftNeighbour)
				bottomRow[i].topLeftNeighbour.bottomRightNeighbour = null;

			bottomRow[i].topLeftNeighbour = null;

			if(isInnerRow)
			{
				bottomRow[i].bottomLeftNeighbour = topRow[i];
				topRow[i].topRightNeighbour = bottomRow[i];

				if((i + 1) != bottomRow.Count)
				{
					bottomRow[i].bottomRightNeighbour = topRow[i + 1];
					topRow[i + 1].topLeftNeighbour = bottomRow[i];
				}
			}
			else
			{
				bottomRow[i].bottomRightNeighbour = topRow[i];
				topRow[i].topLeftNeighbour = bottomRow[i];

				if((i + 1) != bottomRow.Count)
				{
					bottomRow[i + 1].bottomLeftNeighbour = topRow[i];
					topRow[i].topRightNeighbour = bottomRow[i + 1];
				}
			}
        }

		topRight = bottomRow[bottomRow.Count - 1];
	}

	private void MoveTopRowToBottom()
	{
		List<FloorTile> bottomRow = new List<FloorTile>(),
			topRow = new List<FloorTile>();

		FloorTile currentTile = bottomLeft;

		do
		{
			bottomRow.Add(currentTile);
			currentTile = currentTile.rightNeighbour;
		} while(currentTile);

		currentTile = topRight;

		do
		{
			topRow.Add(currentTile);
			currentTile = currentTile.leftNeighbour;
		} while(currentTile);

		Debug.Assert(bottomRow.Count == topRow.Count);

		topRow.Reverse();

		bool isInnerRow = topRight.bottomRightNeighbour != null;

		topRight = isInnerRow ? topRight.bottomRightNeighbour : topRight.bottomLeftNeighbour;

		for(int i = 0; i < bottomRow.Count; ++i)
		{
			Vector3 currentPos = topRow[i].transform.position;
			currentPos.z = bottomRow[i].transform.position.z -
				(FloorTile.Size.y + tileDistance - (FloorTile.Size.x - FloorTile.Size.y));
			topRow[i].transform.position = currentPos;

			if(topRow[i].bottomRightNeighbour)
				topRow[i].bottomRightNeighbour.topLeftNeighbour = null;

			topRow[i].bottomRightNeighbour = null;

			if(topRow[i].bottomLeftNeighbour)
				topRow[i].bottomLeftNeighbour.topRightNeighbour = null;

			topRow[i].bottomLeftNeighbour = null;

			if(!isInnerRow)
			{
				topRow[i].topLeftNeighbour = bottomRow[i];
				bottomRow[i].bottomRightNeighbour = topRow[i];

				if((i + 1) != bottomRow.Count)
				{
					topRow[i].topRightNeighbour = bottomRow[i + 1];
					bottomRow[i + 1].bottomLeftNeighbour = topRow[i];
				}
			}
			else
			{
				topRow[i].topRightNeighbour = bottomRow[i];
				bottomRow[i].bottomLeftNeighbour = topRow[i];

				if((i + 1) != bottomRow.Count)
				{
					topRow[i + 1].topLeftNeighbour = bottomRow[i];
					bottomRow[i].bottomRightNeighbour = topRow[i + 1];
				}
			}
		}

		bottomLeft = topRow[0];
	}
}
