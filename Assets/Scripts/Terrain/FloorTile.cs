﻿using UnityEngine;
using System;

public class FloorTile: Entity
{
	public float middleTileDuckDistance = 0.25f;
	public float sideTileDuckDistance = 0.125f;
	public float duckSpeed = 1.0f;

	/// <summary>
	/// X = forward and backward, longest side
	/// Y = left and right
	/// Z = thickness / up and down
	/// </summary>
	public static Vector3 Size
	{
		get;
		private set;
	}

	[HideInInspector]
	public FloorTile leftNeighbour = null,
		rightNeighbour = null,
		topLeftNeighbour = null,
		topRightNeighbour = null,
		bottomLeftNeighbour = null,
		bottomRightNeighbour = null;

	private bool justReceivedMessage = false;

	protected override void Awake()
	{
		base.Awake();

		if(Size == Vector3.zero)
		{
			MeshFilter meshFilter = GetComponent<MeshFilter>();
			Bounds meshBounds = meshFilter.mesh.bounds;
			Size = meshBounds.size;
		}
	}

	public override void OnEntityReceiveMessage(Entity sender, EntityManager.MessageType messageType, EventArgs arguments)
	{
		if(messageType == EntityManager.MessageType.genericMessage)
		{
			DuckTile();
		}
	}

	public override EntityType Type()
	{
		return EntityType.floorTile;
	}

	private void DuckTile()
	{
		Vector3 position = transform.position;
		position.y = Mathf.Clamp(position.y -= duckSpeed * Time.fixedDeltaTime, -middleTileDuckDistance, float.MaxValue);
		transform.position = position;

		justReceivedMessage = true;

		if(topLeftNeighbour)
			topLeftNeighbour.DuckTileHalf();

		if(topRightNeighbour)
			topRightNeighbour.DuckTileHalf();

		if(bottomLeftNeighbour)
			bottomLeftNeighbour.DuckTileHalf();

		if(bottomRightNeighbour)
			bottomRightNeighbour.DuckTileHalf();

		if(leftNeighbour)
			leftNeighbour.DuckTileHalf();

		if(rightNeighbour)
			rightNeighbour.DuckTileHalf();
	}

	private void DuckTileHalf()
	{
		if(!justReceivedMessage)
		{
			Vector3 position = transform.position;
			position.y = Mathf.Clamp(position.y -= duckSpeed * Time.fixedDeltaTime, -sideTileDuckDistance, float.MaxValue);
			transform.position = position;
		}

		justReceivedMessage = true;
	}

	private void FixedUpdate()
	{
		if(!justReceivedMessage)
		{
			if(transform.position.y < 0)
			{
				Vector3 position = transform.position;
				position.y = Mathf.Clamp(position.y += duckSpeed * Time.fixedDeltaTime, float.MinValue, 0);
				transform.position = position;
			}
		}

		justReceivedMessage = false;
	}
}
