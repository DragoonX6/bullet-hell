﻿using UnityEngine;

namespace VectorExtensions
{
	public static class Vector3Extensions
	{
		public static Vector3 Floor(this Vector3 v)
		{
			return new Vector3(Mathf.Floor(v.x), Mathf.Floor(v.y), Mathf.Floor(v.z));
		}

		public static Vector3 Copy(this Vector3 v)
		{
			return new Vector3(v.x, v.y, v.z);
		}

		public static Vector2 ToVector2XY(this Vector3 v)
		{
			return new Vector2(v.x, v.y);
		}

		public static Vector2 ToVector2XZ(this Vector3 v)
		{
			return new Vector2(v.x, v.z);
		}

		public static Vector2 ToVector2YZ(this Vector3 v)
		{
			return new Vector2(v.y, v.z);
		}
	}
}
