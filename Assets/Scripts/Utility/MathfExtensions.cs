﻿namespace MathfExtensions
{
	public struct EMathf
	{
		public static float Wrap(float value, float lower, float upper)
		{
			if(upper == lower)
				return lower;
			while(value < lower)
				value += upper;
			while(value > upper)
				value -= upper;
			return value;
		}
	}
}
