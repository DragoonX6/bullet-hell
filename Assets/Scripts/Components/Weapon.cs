﻿using UnityEngine;
using System.Collections.Generic;

public class Weapon: MonoBehaviour
{
	public int damage = 0;
	public float projectileSpeed = 0;
	public LayerMask bulletLayer = 0;
	public LayerMask bulletCollisionLayer = 0;
	public List<ProjectileShooter> projectileShooters = new List<ProjectileShooter>();

	private Entity owningEntity = null;

	private void Awake()
	{
		int layer = (int)Mathf.Log(bulletLayer.value, 2);
		for(int i = 0; i < projectileShooters.Count; ++i)
		{
			projectileShooters[i].bulletLayer = layer;
			projectileShooters[i].bulletCollisionLayer = bulletCollisionLayer;
			projectileShooters[i].damage = damage;
			projectileShooters[i].projectileSpeed = projectileSpeed;
		}
	}

	public void SetOwner(Entity owner)
	{
		owningEntity = owner;

		for(int i = 0; i < projectileShooters.Count; ++i)
		{
			projectileShooters[i].owningEntity = owningEntity;
		}
	}

	public void Fire()
	{
		for(int i = 0; i < projectileShooters.Count; ++i)
		{
			projectileShooters[i].Fire();
		}
	}
}
