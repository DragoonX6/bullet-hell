﻿using UnityEngine;

public class FollowObject: MonoBehaviour
{
	public Vector3 distances = Vector3.zero;
	public GameObject target = null;

	private void LateUpdate()
	{
		transform.position = target.transform.position;
		transform.position += distances;
	}
}
