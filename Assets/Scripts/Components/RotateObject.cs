﻿using UnityEngine;

public class RotateObject : MonoBehaviour
{
	public float rotationSpeed = 0.0f;

	public enum RotationAxis
	{
		up,
		right,
		forward
	}

	public RotationAxis rotationAxis = RotationAxis.up;

	private void Update()
	{
		if(Game.Instance.paused)
			return;

		switch(rotationAxis)
		{
			case RotationAxis.up:
				transform.Rotate(transform.up, rotationSpeed * Time.deltaTime);
			break;
			case RotationAxis.right:
				transform.Rotate(transform.right, rotationSpeed * Time.deltaTime);
			break;
			case RotationAxis.forward:
				transform.Rotate(transform.forward, rotationSpeed * Time.deltaTime);
			break;
		}
	}
}
