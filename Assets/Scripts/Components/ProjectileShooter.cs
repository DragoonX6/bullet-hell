﻿using UnityEngine;

public class ProjectileShooter: MonoBehaviour
{
	public int damage = 0;
	public float fireRate = 0.0f;
	public float projectileSpeed = 0;

	private float fireTimer = 0.0f;

	private SpawnPool spawnPool = null;

	public Entity owningEntity = null;
	public int bulletLayer = 0;
	public int bulletCollisionLayer = 0;

	public enum ShotDirection
	{
		forward,
		back,
		left,
		right
	}

	public ShotDirection shotDirection = ShotDirection.forward;

	private void Awake()
	{
		if(fireRate == 0.0f)
		{
			fireRate = 1.0f;
		}

		spawnPool = Game.Instance.SpawnPool;
	}

	public void Fire()
	{
		if(fireTimer <= float.Epsilon)
		{
			Projectile bullet = spawnPool.Get(
				Entity.EntityType.projectile, transform.position, 1)[0] as Projectile;
			bullet.gameObject.layer = bulletLayer;
			bullet.collisionLayer = bulletCollisionLayer;
			bullet.owningEntity = owningEntity;
			bullet.damage = damage;
			bullet.speed = projectileSpeed;

			switch(shotDirection)
			{
				case ShotDirection.forward:
					bullet.movementDirection = transform.forward;
				break;
				case ShotDirection.back:
					bullet.movementDirection = -transform.forward;
				break;
				case ShotDirection.right:
					bullet.movementDirection = transform.right;
				break;
				case ShotDirection.left:
					bullet.movementDirection = -transform.right;
				break;
			}

			fireTimer += 1F / fireRate;
		}
	}

	private void Update()
	{
		fireTimer = Mathf.Clamp(fireTimer - Time.deltaTime, 0.0f, float.MaxValue);
	}
}
