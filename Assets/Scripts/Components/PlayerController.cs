﻿using UnityEngine;
using InControl;

[RequireComponent(typeof(InputComponent)), RequireComponent(typeof(PlayerEntity))]
public class PlayerController: MonoBehaviour
{
	private InputComponent input;
	private PlayerEntity player;
	private FloorController floorController;

	#region Input
	private PlayerControlSet pcs;
	public float leftStickDeadZone = 0.3f;
	public float rightStickDeadZone = 0.3f;
	#endregion

	public float movementSpeed = 5.0f;

	private void Awake()
	{
		input = gameObject.GetComponent<InputComponent>();
		player = gameObject.GetComponent<PlayerEntity>();
		floorController = Game.Instance.FloorController;

		pcs = new PlayerControlSet();

		input.BindAxis(pcs.leftStick, ActionBinder.Bind<PlayerController,
			Vector2>(this, (x, value) => x.OnLeftStick(value)));
		input.BindAxis(pcs.rightStick, ActionBinder.Bind<PlayerController,
			Vector2>(this, (x, value) => x.OnRightStick(value)));
		input.BindAction(pcs.rightTrigger, InputComponent.State.pressAndHold,
			ActionBinder.Bind(player, x => x.OnRightTrigger()));
		input.BindAction(pcs.startButton, InputComponent.State.pressed,
			ActionBinder.Bind(this, x => x.OnStartButtonPressed()));
	}

	private void OnLeftStick(Vector2 value)
	{
		value.Normalize();

		if(value.magnitude >= leftStickDeadZone)
		{
			transform.position += (Vector3.forward * value.y *
				movementSpeed * Time.deltaTime);
			transform.position += (Vector3.right * value.x *
				movementSpeed * Time.deltaTime);

			floorController.OnPlayerMove(transform.position);
		}
	}

	private void OnRightStick(Vector2 value)
	{
		value.Normalize();

		if(value.magnitude >= rightStickDeadZone)
		{
			transform.rotation = Quaternion.Euler(new Vector3(0,
				(Mathf.Atan2(value.x, value.y) * Mathf.Rad2Deg), 0));
		}
	}

	private void OnStartButtonPressed()
	{
		Game.Instance.paused = true;
	}
}

class PlayerControlSet: PlayerActionSet
{
	private PlayerAction LSLeft;
	private PlayerAction LSRight;
	private PlayerAction LSUp;
	private PlayerAction LSDown;
	public PlayerTwoAxisAction leftStick;

	private PlayerAction RSLeft;
	private PlayerAction RSRight;
	private PlayerAction RSUp;
	private PlayerAction RSDown;
	public PlayerTwoAxisAction rightStick;

	public PlayerAction rightTrigger;

	public PlayerAction startButton;

	public PlayerControlSet()
	{
		LSLeft = CreatePlayerAction("LeftStick_Left");
		LSLeft.AddDefaultBinding(InputControlType.LeftStickLeft);
		LSRight = CreatePlayerAction("LeftStick_Right");
		LSRight.AddDefaultBinding(InputControlType.LeftStickRight);
		LSUp = CreatePlayerAction("LeftStick_Up");
		LSUp.AddDefaultBinding(InputControlType.LeftStickUp);
		LSDown = CreatePlayerAction("LeftStick_Down");
		LSDown.AddDefaultBinding(InputControlType.LeftStickDown);

		leftStick = CreateTwoAxisPlayerAction(LSLeft, LSRight, LSDown, LSUp);

		RSLeft = CreatePlayerAction("RightStick_Left");
		RSLeft.AddDefaultBinding(InputControlType.RightStickLeft);
		RSRight = CreatePlayerAction("RightStick_Right");
		RSRight.AddDefaultBinding(InputControlType.RightStickRight);
		RSUp = CreatePlayerAction("RightStick_Up");
		RSUp.AddDefaultBinding(InputControlType.RightStickUp);
		RSDown = CreatePlayerAction("RightStick_Down");
		RSDown.AddDefaultBinding(InputControlType.RightStickDown);

		rightStick = CreateTwoAxisPlayerAction(RSLeft, RSRight, RSDown, RSUp);

		rightTrigger = CreatePlayerAction("Right_Trigger");
		rightTrigger.AddDefaultBinding(InputControlType.RightTrigger);

		startButton = CreatePlayerAction("Game_Start");
		startButton.AddDefaultBinding(InputControlType.Start);
	}
}
