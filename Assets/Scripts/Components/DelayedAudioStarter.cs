﻿using UnityEngine;

/// <summary>
/// Plays an audio clip after the scene has finished loading
/// Controlled by execution order.
/// </summary>
[RequireComponent(typeof(AudioSource))]
public class DelayedAudioStarter: MonoBehaviour
{
	private void Awake()
	{
		AudioSource audio = GetComponent<AudioSource>();
		audio.Play();
	}
}
