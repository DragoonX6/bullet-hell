﻿using UnityEngine;
using MathfExtensions;

public class SpawnEnemiesArea: MonoBehaviour
{
	[SerializeField]
	private GameObject target = null;

	[SerializeField]
	private Vector2 startPoint = Vector2.zero;
	[SerializeField]
	private Vector2 endPoint = Vector2.zero;

	[System.Serializable]
	private class WeightedEntityTypeData
	{
		public Entity.EntityType type = Entity.EntityType.none;
		public float height = 0;
		public int weight = 0;
	}

	[SerializeField]
	private WeightedEntityTypeData[] weightedEntities = null;
	private int totalWeight = 0;

	[SerializeField]
	private float spawnInterval = 0f;
	private float spawnCounter = 0f;

	[SerializeField]
	private int randomSeed = 0;
	private XXHash randomEngine = null;
	private int enemyCounter = 0;

	public int enemyCount = 0;
	[SerializeField]
	private int maxEnemies = 0;

	private void Awake()
	{
		if(!target)
			throw new System.ArgumentNullException("Must have a target");

		if(weightedEntities != null)
		{
			for(int i = 0; i < weightedEntities.Length; ++i)
			{
				totalWeight += weightedEntities[i].weight;
			}
		}

		randomEngine = new XXHash(randomSeed);
	}

	private void LateUpdate()
	{
		if(Game.Instance.paused)
			return;

		transform.position = target.transform.position;

		if(enemyCount >= maxEnemies)
			return;

		if(spawnCounter > spawnInterval && spawnInterval != 0)
		{
			int weight = randomEngine.Range(0, totalWeight, enemyCounter);

			WeightedEntityTypeData weightedEntity = null;
			for(int currentWeight = weight, i = 0; currentWeight >= 0;
				i = (int)EMathf.Wrap(++i, 0, weightedEntities.Length - 1))
			{
				currentWeight -= weightedEntities[i].weight;

				if(currentWeight < weightedEntities[i].weight)
				{
					weightedEntity = weightedEntities[i];
					break;
				}
			}

			float xPosition = randomEngine.Range(target.transform.position.x +
				startPoint.x, target.transform.position.x + endPoint.x, enemyCounter);
			float zPosition = randomEngine.Range(target.transform.position.z +
				startPoint.y, target.transform.position.z + endPoint.y, enemyCounter);

			Vector3 spawnPosition = new Vector3(xPosition, weightedEntity.height,
				zPosition);

			Game.Instance.SpawnPool.Get(weightedEntity.type, spawnPosition, 1);

			++enemyCounter;
			++enemyCount;

			spawnCounter -= spawnInterval;

			return;
		}

		spawnCounter += Time.deltaTime;
	}

	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.green;

		Vector3 center = new Vector3(target.transform.position.x +
			startPoint.x + ((endPoint.x - startPoint.x) * 0.5f),
			target.transform.position.y,
			target.transform.position.z +
			startPoint.y + ((endPoint.y - startPoint.y) * 0.5f));

		Vector3 size = new Vector3(endPoint.x - startPoint.x, 0,
			endPoint.y - startPoint.y);

		Gizmos.DrawWireCube(center, size);
	}
}
