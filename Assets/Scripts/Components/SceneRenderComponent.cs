﻿using UnityEngine;

[ExecuteInEditMode, RequireComponent(typeof(Camera))]
public class SceneRenderComponent: MonoBehaviour
{
	public RenderTexture renderTex = null;
	public Material renderMat = null;

	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		if(renderTex == null)
			throw new System.NullReferenceException("renderTex cannot be null.");

		if(renderMat == null)
			throw new System.NullReferenceException("renderMat cannot be null");

		Graphics.Blit(renderTex, source, renderMat);
		Graphics.Blit(source, destination);
	}
}
