﻿using UnityEngine;
using System.Collections.Generic;

public class SpawnPool: MonoBehaviour
{
	private Dictionary<Entity.EntityType, List<Tuple<Entity, bool>>> pool = new Dictionary<Entity.EntityType, List<Tuple<Entity, bool>>>();
	
	#region Serialization
	private void Awake()
	{
		Deserialize();
	}

	[System.Serializable]
	private class TypeList
	{
		public Entity.EntityType type = Entity.EntityType.none;
		public List<EntityPair> entities = new List<EntityPair>();
	}

	[System.Serializable]
	private class EntityPair
	{
		public bool spawned = false;
		public Entity entity = null;
	}

	[SerializeField]
	private List<TypeList> Pool = new List<TypeList>();

	private void Deserialize()
	{
		pool = new Dictionary<Entity.EntityType, List<Tuple<Entity, bool>>>();
		for(int i = 0; i < Pool.Count; ++i)
		{
			if(!pool.ContainsKey(Pool[i].type))
				pool.Add(Pool[i].type, new List<Tuple<Entity, bool>>());

			for(int j = 0; j < Pool[i].entities.Count; ++j)
			{
				pool[Pool[i].type].Add(Tuple.Create(Pool[i].entities[j].entity, Pool[i].entities[j].spawned));
			}
		}
	}

	private void OnValidate()
	{
		for(int i = 0; i < Pool.Count; ++i)
		{
			for(int j = Pool[i].entities.Count - 1; j >= 0; --j)
			{
				if(Pool[i].entities[j].entity == null)
					continue;

				if(Pool[i].entities[j].entity.Type() != Pool[i].type)
					Pool[i].entities.RemoveAt(j);
			}
		}
	}
	#endregion

	public Entity[] Get(Entity.EntityType typeID, Vector3 position, int count)
	{
		Entity[] ret = new Entity[count];

		if(pool.ContainsKey(typeID))
		{
			List<Tuple<Entity, bool>> pooledEntities = pool[typeID];
			for(int i = 0; i < pooledEntities.Count; ++i)
			{
				if(!pooledEntities[i].Item2)
				{
					pooledEntities[i].Item1.gameObject.SetActive(true);
					pooledEntities[i].Item1.spawned = true;
					pooledEntities[i].Item1.transform.position = position;
					pooledEntities[i].Item2 = true;

					pooledEntities[i].Item1.transform.parent = null;
					ret[count - 1] = pooledEntities[i].Item1;

					if(--count <= 0)
						return ret;
				}
			}

			for(; count > 0; --count)
			{
				Entity temp = EntityManager.Factory(typeID);
				if(temp == null)
				{
					Debug.LogError("Error: No factory found for " + typeID);
					return ret;
				}
				temp.transform.position = position;
				temp.spawned = true;
				ret[count - 1] = temp;
				pooledEntities.Add(Tuple.Create(temp, true));
			}
		}
		else
		{
			pool.Add(typeID, new List<Tuple<Entity, bool>>());
			List<Tuple<Entity, bool>> pooledEntities = pool[typeID];
			for(; count > 0; --count)
			{
				Entity temp = EntityManager.Factory(typeID);
				if(temp == null)
				{
					Debug.LogError("Error: No factory found for " + typeID);
					return ret;
				}
				temp.transform.position = position;
				temp.spawned = true;
				ret[count - 1] = temp;
				pooledEntities.Add(Tuple.Create(temp, true));
			}
		}

		return ret;
	}

	public void PutBack(Entity spawnedObject)
	{
		Entity.EntityType typeID = spawnedObject.Type();
		if(pool.ContainsKey(typeID))
		{
			List<Tuple<Entity, bool>> entry = pool[typeID];
			int index = entry.FindIndex(x => x.Item1 == spawnedObject);
			if(index >= 0)
			{
				Tuple<Entity, bool> temp = entry[index];
				temp.Item1.transform.position = transform.position;
				temp.Item1.transform.parent = transform;
				temp.Item2 = false;
				temp.Item1.gameObject.SetActive(false);
				temp.Item1.spawned = false;
				entry[index] = temp;
			}
			else
			{
				Tuple<Entity, bool> temp = Tuple.Create(spawnedObject, false);
				temp.Item1.transform.position = transform.position;
				temp.Item1.transform.parent = transform;
				temp.Item1.gameObject.SetActive(false);
				temp.Item1.spawned = false;
				entry.Add(temp);
			}

			return;
		}

		pool.Add(typeID, new List<Tuple<Entity, bool>>());
		spawnedObject.gameObject.SetActive(false);
		spawnedObject.transform.position = transform.position;
		spawnedObject.transform.parent = transform;
		spawnedObject.spawned = false;
		pool[typeID].Add(Tuple.Create(spawnedObject, false));
	}
}
