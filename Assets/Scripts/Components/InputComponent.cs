﻿using UnityEngine;
using InControl;
using System;
using System.Collections.Generic;

public class InputComponent: MonoBehaviour
{
	public bool ignorePaused = false;

	public enum State
	{
		pressed,
		released,
		pressAndHold
	}

	private Dictionary<Tuple<PlayerAction, State>, Action> actions;
	private Dictionary<PlayerOneAxisAction, Action<float>> axes;
	private Dictionary<PlayerTwoAxisAction, Action<Vector2>> dualAxes;

	public InputComponent()
	{
		actions = new Dictionary<Tuple<PlayerAction, State>, Action>();
		axes = new Dictionary<PlayerOneAxisAction, Action<float>>();
		dualAxes = new Dictionary<PlayerTwoAxisAction, Action<Vector2>>();
	}

	public void BindAction(PlayerAction action, State state, Action callBack)
	{
		Tuple<PlayerAction, State> actionPair = Tuple.Create(action, state);
		if(actions.ContainsKey(actionPair))
		{
			actions[actionPair] += callBack;
		}
		else
		{
			actions.Add(actionPair, callBack);
		}
	}

	public void RemoveActionBinding(PlayerAction action, State state, Action callBack)
	{
		Tuple<PlayerAction, State> actionPair = Tuple.Create(action, state);
		if(actions.ContainsKey(actionPair))
			actions[actionPair] -= callBack;
	}

	public void RemoveAllActionBindings()
	{
		actions.Clear();
	}

	public void RemoveAllBindingsFromAction(PlayerAction action, State state)
	{
		Tuple<PlayerAction, State> actionPair = Tuple.Create(action, state);
		if(actions.ContainsKey(actionPair))
			actions.Remove(actionPair);
    }

	public void BindAxis(PlayerOneAxisAction axis, Action<float> callBack)
	{
		if(axes.ContainsKey(axis))
		{
			axes[axis] += callBack;
		}
		else
		{
			axes.Add(axis, callBack);
		}
	}

	public void RemoveAxisBinding(PlayerOneAxisAction axis, Action<float> callBack)
	{
		if(axes.ContainsKey(axis))
		{
			axes[axis] -= callBack;
		}
	}

	public void RemoveAllAxisBindings()
	{
		axes.Clear();
		dualAxes.Clear();
	}

	public void RemoveAllBindingsFromAxis(PlayerOneAxisAction axis)
	{
		axes.Remove(axis);
	}

	public void RemoveAllBindings()
	{
		actions.Clear();
		axes.Clear();
		dualAxes.Clear();
	}

	public void BindAxis(PlayerTwoAxisAction axes, Action<Vector2> callBack)
	{
		if(dualAxes.ContainsKey(axes))
		{
			dualAxes[axes] += callBack;
		}
		else
		{
			dualAxes.Add(axes, callBack);
		}
	}

	public void RemoveAxisBinding(PlayerTwoAxisAction axes, Action<Vector2> callBack)
	{
		if(dualAxes.ContainsKey(axes))
		{
			dualAxes[axes] -= callBack;
		}
	}

	public void RemoveAllBindingsFromAxis(PlayerTwoAxisAction axes)
	{
		dualAxes.Remove(axes);
	}

	private void Update()
	{
		if(!ignorePaused)
			if(Game.Instance.paused)
				return;

		Dictionary<Tuple<PlayerAction, State>, Action>.Enumerator actionIt =
			actions.GetEnumerator();
		while(actionIt.MoveNext())
		{
			KeyValuePair<Tuple<PlayerAction, State>, Action> entry = actionIt.Current;
			if(entry.Key.Item1.HasChanged)
			{
				switch(entry.Key.Item2)
				{
					case State.pressed:
					{
						if(entry.Key.Item1.IsPressed)
							entry.Value();
					}
					break;
					case State.released:
					{
						if(!entry.Key.Item1.IsPressed)
							entry.Value();
					}
					break;
				}
			}

			if(entry.Key.Item2 == State.pressAndHold)
			{
				if(entry.Key.Item1.IsPressed)
					entry.Value();
			}
		}

		Dictionary<PlayerOneAxisAction, Action<float>>.Enumerator axesIt =
			axes.GetEnumerator();
		while(axesIt.MoveNext())
		{
			KeyValuePair<PlayerOneAxisAction, Action<float>> entry = axesIt.Current;
			entry.Value(entry.Key);
		}

		Dictionary<PlayerTwoAxisAction, Action<Vector2>>.Enumerator dualAxesIt =
			dualAxes.GetEnumerator();
		while(dualAxesIt.MoveNext())
		{
			KeyValuePair<PlayerTwoAxisAction, Action<Vector2>> entry = dualAxesIt.Current;
			entry.Value(entry.Key);
		}
	}
}
