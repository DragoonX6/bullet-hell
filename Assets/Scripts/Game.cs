﻿using UnityEngine;

public class Game: MonoBehaviour
{
	public bool paused = false;

	private static Game instance = null;
	public static Game Instance
	{
		get
		{
			if(instance == null)
			{
				new GameObject("Game").AddComponent<Game>();
			}

			return instance;
		}
	}

	public Game()
	{
		instance = this;
	}

	[SerializeField]
	private EntityManager entityManager = null;
	public EntityManager EntityManager
	{
		get
		{
			if(entityManager == null)
			{
				entityManager = GetComponent<EntityManager>();
				if(entityManager == null)
					entityManager = gameObject.AddComponent<EntityManager>();
			}

			return entityManager;
		}
	}

	[SerializeField]
	private SpawnPool spawnPool = null;
	public SpawnPool SpawnPool
	{
		get
		{
			if(spawnPool == null)
			{
				spawnPool = GetComponent<SpawnPool>();
				if(spawnPool == null)
					spawnPool = gameObject.AddComponent<SpawnPool>();
			}

			return spawnPool;
		}
	}

	[SerializeField]
	private FloorController floorController = null;
	public FloorController FloorController
	{
		get
		{
			if(floorController == null)
			{
				floorController = FindObjectOfType<FloorController>();
				if(floorController == null)
					throw new MissingReferenceException(
						"Can't find FloorController object");
			}

			return floorController;
		}
	}

	[SerializeField]
	private Camera renderCamera = null;
	public Camera RenderCamera
	{
		get
		{
			if(renderCamera == null)
			{
				Camera[] cameras = FindObjectsOfType<Camera>();
				for(int i = 0; i < cameras.Length; ++i)
				{
					if(cameras[i].name == "Render Camera")
						renderCamera = cameras[i];
				}

				if(renderCamera == null)
					throw new MissingReferenceException(
						"Can't find the render camera.");
			}

			return renderCamera;
		}
	}

	[SerializeField]
	private SpawnEnemiesArea enemySpawner = null;
	public SpawnEnemiesArea EnemySpawner
	{
		get
		{
			if(enemySpawner == null)
			{
				enemySpawner = FindObjectOfType<SpawnEnemiesArea>();

				if(enemySpawner == null)
					throw new MissingReferenceException("Can't find the enemy spawner.");
			}

			return enemySpawner;
		}
	}

	[SerializeField]
	private HighScoreManager highScoreManager = null;
	public HighScoreManager HighScoreManager
	{
		get
		{
			if(highScoreManager == null)
			{
				highScoreManager = GetComponent<HighScoreManager>();

				if(highScoreManager == null)
					highScoreManager = gameObject.AddComponent<HighScoreManager>();
			}

			return highScoreManager;
		}
	}
}
