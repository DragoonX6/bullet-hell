﻿using UnityEditor;

[CustomEditor(typeof(PlayerEntity))]
public class PlayerEntityEditor : Editor
{
	public override void OnInspectorGUI()
	{
		EditorGUI.BeginChangeCheck();

		SerializedProperty serializedMovementSpeed =
			serializedObject.FindProperty("movementSpeed");

		var it = serializedObject.GetIterator();
		bool goInside = true;
		while(it.NextVisible(goInside))
		{
			goInside = false;

			if(it.name == serializedMovementSpeed.name)
				continue;

			EditorGUILayout.PropertyField(it, true);
		}

		if(EditorGUI.EndChangeCheck())
			serializedObject.ApplyModifiedProperties();
	}
}
