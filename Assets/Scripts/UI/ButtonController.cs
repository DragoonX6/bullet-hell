﻿using UnityEngine;

public class ButtonController : MonoBehaviour
{
	private enum Selector
	{
		none,
		previous,
		next
	}

	public Button[] buttons;

	private int selectedButton = 0;

	[SerializeField]
	private float selectionTimeTreshold = 0.3f;
	private float selectionTimer = 0f;

	private Selector lastSelector = Selector.none;

	public void ResetSelection()
	{
		OnEnable();
	}

	public void ResetTimer()
	{
		selectionTimer = 0f;
		lastSelector = Selector.none;
	}

	private void OnEnable()
	{
		buttons[selectedButton].Deselect();
		selectedButton = 0;
		selectionTimer = 0f;
		lastSelector = Selector.none;
		buttons[selectedButton].Select();
	}

	public void SelectPreviousButton()
	{
		if(selectionTimer < selectionTimeTreshold && lastSelector == Selector.previous)
		{
			return;
		}

		lastSelector = Selector.previous;

		buttons[selectedButton].Deselect();

		if(selectedButton == 0)
			selectedButton = buttons.Length - 1;
		else
			--selectedButton;

		selectionTimer = 0;
		buttons[selectedButton].Select();
	}

	public void SelectNextButton()
	{
		if(selectionTimer < selectionTimeTreshold && lastSelector == Selector.next)
		{
			return;
		}

		lastSelector = Selector.next;

		buttons[selectedButton].Deselect();

		if(selectedButton == buttons.Length - 1)
			selectedButton = 0;
		else
			++selectedButton;

		selectionTimer = 0;
		buttons[selectedButton].Select();
	}

	public Button GetSelectedButton()
	{
		return buttons[selectedButton];
	}

	private void Update()
	{
		if(selectionTimer < selectionTimeTreshold)
			selectionTimer += Time.deltaTime;
	}
}
