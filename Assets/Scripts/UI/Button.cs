﻿using UnityEngine;

public abstract class Button: MonoBehaviour
{
	[SerializeField]
	private GameObject selectedBackground = null;
	[SerializeField]
	private GameObject deselectedBackground = null;

	private void Awake ()
	{
		if(selectedBackground == null)
			throw new MissingReferenceException("Missing selectedBackground");
		if(deselectedBackground == null)
			throw new MissingReferenceException("Missing deselectedBackground");
	}

	public void Select()
	{
		selectedBackground.SetActive(true);
		deselectedBackground.SetActive(false);
	}

	public void Deselect()
	{
		selectedBackground.SetActive(false);
		deselectedBackground.SetActive(true);
	}

	public abstract void OnPress();
}
