﻿using UnityEngine;
using InControl;
using System.Collections;

public class ConfirmationPanelController: MonoBehaviour
{
	private InputComponent input = null;

	[SerializeField]
	private InputComponent parentInput = null;

	private ConfirmationPanelControlSet pcs;

	[SerializeField]
	private ButtonController buttonController = null;

	public float moveTreshold = 0.3f;

	private bool firstFrame = true;

	private void Awake()
	{
		input = GetComponent<InputComponent>();
		input.enabled = false;

		if(parentInput == null)
			throw new MissingReferenceException("Missing parent input controller");

		if(buttonController == null)
			throw new MissingReferenceException("Missing button controller");

		pcs = new ConfirmationPanelControlSet();

		input.BindAction(pcs.confirmButton, InputComponent.State.pressed,
			ActionBinder.Bind(this, x => x.OnConfirmButtonPressed()));
		input.BindAction(pcs.backButton, InputComponent.State.pressed,
			ActionBinder.Bind(this, x => x.OnBackButtonPressed()));
		input.BindAxis(pcs.leftStick,
			ActionBinder.Bind<ConfirmationPanelController, Vector2>(this,
			(x, value) => x.OnLeftStick(value)));
	}

	private void OnEnable()
	{
		if(firstFrame)
			return;

		if(input.enabled == false)
			input.enabled = true;

		if(parentInput.enabled == true)
			parentInput.enabled = false;
	}

	private IEnumerator Start()
	{
		gameObject.SetActive(false);

		firstFrame = false;

		yield return new WaitForEndOfFrame();

		input.enabled = true;
	}

	private void OnDisable()
	{
		parentInput.enabled = true;
	}

	private void OnLeftStick(Vector2 value)
	{
		if(value.x >= moveTreshold)
		{
			buttonController.SelectPreviousButton();
		}
		else if(value.x <= -moveTreshold)
		{
			buttonController.SelectNextButton();
		}
		else
		{
			buttonController.ResetTimer();
		}
	}

	private void OnConfirmButtonPressed()
	{
		buttonController.GetSelectedButton().OnPress();
	}

	private void OnBackButtonPressed()
	{
		input.enabled = false;
		parentInput.enabled = true;
		buttonController.ResetSelection();
		gameObject.SetActive(false);
	}
}

class ConfirmationPanelControlSet: PlayerActionSet
{
	private PlayerAction LSLeft;
	private PlayerAction LSRight;
	private PlayerAction LSUp;
	private PlayerAction LSDown;
	public PlayerTwoAxisAction leftStick;

	public PlayerAction confirmButton;
	public PlayerAction backButton;

	public ConfirmationPanelControlSet()
	{
		LSLeft = CreatePlayerAction("Confirm_LeftStick_Left");
		LSLeft.AddDefaultBinding(InputControlType.LeftStickLeft);
		LSRight = CreatePlayerAction("Confirm_LeftStick_Right");
		LSRight.AddDefaultBinding(InputControlType.LeftStickRight);
		LSUp = CreatePlayerAction("Confirm_LeftStick_Up");
		LSUp.AddDefaultBinding(InputControlType.LeftStickUp);
		LSDown = CreatePlayerAction("Confirm_LeftStick_Down");
		LSDown.AddDefaultBinding(InputControlType.LeftStickDown);

		leftStick = CreateTwoAxisPlayerAction(LSLeft, LSRight, LSDown, LSUp);

		confirmButton = CreatePlayerAction("Confirm_Confirm");
		confirmButton.AddDefaultBinding(InputControlType.Action1);

		backButton = CreatePlayerAction("Confirm_Back");
		backButton.AddDefaultBinding(InputControlType.Action2);
	}
}
