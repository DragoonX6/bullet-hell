﻿using UnityEngine;
using InControl;
using System.Collections;

[RequireComponent(typeof(InputComponent))]
public class MainMenuController: MonoBehaviour
{
	private InputComponent input = null;
	private MainMenuControlSet mcs;

	[SerializeField]
	private ButtonController buttonController = null;

	public float moveTreshold = 0.3f;

	public float inputBlockTreshold = 0.3f;
	private float inputBlockTimer = 0;

	private bool blockInput = true;

	private bool firstFrame = true;

	private void Awake()
	{
		input = GetComponent<InputComponent>();
		input.enabled = false;

		if(buttonController == null)
			throw new MissingReferenceException("Missing button controller");

		mcs = new MainMenuControlSet();

		input.BindAction(mcs.confirmButton, InputComponent.State.pressed,
			ActionBinder.Bind(this, x => x.OnConfirmButtonPressed()));
		input.BindAxis(mcs.leftStick, ActionBinder.Bind<MainMenuController, Vector2>(this,
			(x, value) => x.OnLeftStick(value)));
	}

	private void OnEnable()
	{
		if(firstFrame)
			return;

		if(input.enabled == false)
			input.enabled = true;

		blockInput = true;
		inputBlockTimer = 0;
	}

	private IEnumerator Start()
	{
		firstFrame = false;

		yield return new WaitForEndOfFrame();

		input.enabled = true;
	}

	private void OnLeftStick(Vector2 value)
	{
		if(value.y >= moveTreshold && !blockInput)
		{
			buttonController.SelectPreviousButton();
		}
		else if(value.y <= -moveTreshold && !blockInput)
		{
			buttonController.SelectNextButton();
		}
		else if(blockInput && (value.y <= moveTreshold && value.y >= -moveTreshold))
		{
			blockInput = false;
		}
		else
		{
			buttonController.ResetTimer();
		}
	}

	private void OnConfirmButtonPressed()
	{
		buttonController.GetSelectedButton().OnPress();
	}

	private void Update()
	{
		if(inputBlockTimer < inputBlockTreshold && blockInput)
			inputBlockTimer += Time.deltaTime;
		else
		{
			blockInput = false;
			enabled = false;
		}
	}
}

public class MainMenuControlSet: PlayerActionSet
{
	private PlayerAction LSLeft;
	private PlayerAction LSRight;
	private PlayerAction LSUp;
	private PlayerAction LSDown;
	public PlayerTwoAxisAction leftStick;

	public PlayerAction confirmButton;

	public MainMenuControlSet()
	{
		LSLeft = CreatePlayerAction("MainMenu_LeftStick_Left");
		LSLeft.AddDefaultBinding(InputControlType.LeftStickLeft);
		LSRight = CreatePlayerAction("MainMenu_LeftStick_Right");
		LSRight.AddDefaultBinding(InputControlType.LeftStickRight);
		LSUp = CreatePlayerAction("MainMenu_LeftStick_Up");
		LSUp.AddDefaultBinding(InputControlType.LeftStickUp);
		LSDown = CreatePlayerAction("MainMenu_LeftStick_Down");
		LSDown.AddDefaultBinding(InputControlType.LeftStickDown);

		leftStick = CreateTwoAxisPlayerAction(LSLeft, LSRight, LSDown, LSUp);

		confirmButton = CreatePlayerAction("MainMenu_Confirm");
		confirmButton.AddDefaultBinding(InputControlType.Action1);
	}
}
