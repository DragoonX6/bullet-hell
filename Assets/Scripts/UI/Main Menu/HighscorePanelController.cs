﻿using UnityEngine;
using UnityEngine.UI;
using InControl;
using System.Collections;
using System.Collections.ObjectModel;

[RequireComponent(typeof(InputComponent))]
public class HighscorePanelController: MonoBehaviour
{
	private InputComponent input = null;

	[SerializeField]
	private InputComponent parentInput = null;

	private HighscorePanelControlSet pcs;

	[SerializeField]
	private ButtonController buttonController = null;

	private bool firstFrame = true;

	[SerializeField]
	private Text[] scoreTextObjects = null;

	private void Awake()
	{
		input = GetComponent<InputComponent>();
		input.enabled = false;

		if(parentInput == null)
			throw new MissingReferenceException("Missing parent input controller");

		if(buttonController == null)
			throw new MissingReferenceException("Missing button controller");

		if(scoreTextObjects == null)
			throw new MissingReferenceException("FATAL: missing score text objects");

		ReadOnlyCollection<int> scores = Game.Instance.HighScoreManager.GetHighScores();

		if(scoreTextObjects.Length != scores.Count)
			throw new System.ArgumentOutOfRangeException(
				string.Format(@"The amount of score text objects ({0}) doesn't match the 
				amount of high scores ({1})", scoreTextObjects.Length, scores.Count));

		for(int i = 0; i < scores.Count; ++i)
		{
			scoreTextObjects[i].text = scores[i].ToString();
		}

		pcs = new HighscorePanelControlSet();

		input.BindAction(pcs.confirmButton, InputComponent.State.pressed,
			ActionBinder.Bind(this, x => x.OnConfirmButtonPressed()));
		input.BindAction(pcs.backButton, InputComponent.State.pressed,
			ActionBinder.Bind(this, x => x.OnBackButtonPressed()));
	}

	private void OnEnable()
	{
		if(firstFrame)
			return;

		if(input.enabled == false)
			input.enabled = true;

		if(parentInput.enabled == true)
			parentInput.enabled = false;
	}

	private IEnumerator Start()
	{
		gameObject.SetActive(false);

		firstFrame = false;

		yield return new WaitForEndOfFrame();

		input.enabled = true;
	}

	private void OnDisable()
	{
		parentInput.enabled = true;
	}

	private void OnBackButtonPressed()
	{
		input.enabled = false;
		parentInput.enabled = true;
		buttonController.ResetSelection();
		gameObject.SetActive(false);
	}

	private void OnConfirmButtonPressed()
	{
		buttonController.GetSelectedButton().OnPress();
	}
}

public class HighscorePanelControlSet: PlayerActionSet
{
	public PlayerAction confirmButton;
	public PlayerAction backButton;

	public HighscorePanelControlSet()
	{
		confirmButton = CreatePlayerAction("Highscore_Confirm");
		confirmButton.AddDefaultBinding(InputControlType.Action1);

		backButton = CreatePlayerAction("Highscore_Back");
		backButton.AddDefaultBinding(InputControlType.Action2);
	}
}
