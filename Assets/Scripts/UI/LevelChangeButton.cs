﻿using System;
using UnityEngine;

public class LevelChangeButton: Button
{
	[SerializeField]
	private string levelName = null;

	private void Awake()
	{
		if(levelName == null || levelName == string.Empty)
			throw new ArgumentException("Missing level name");
	}

	public override void OnPress()
	{
		Application.LoadLevel(levelName);
	}
}
