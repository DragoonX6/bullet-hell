﻿using UnityEngine;

public class ContinueButton: Button
{
	[SerializeField]
	private InputComponent pauseMenuInput = null;

	private void Awake()
	{
		if(pauseMenuInput == null)
			throw new MissingReferenceException("Missing pause menu input component");
	}

	public override void OnPress()
	{
		pauseMenuInput.enabled = false;
		Game.Instance.paused = false;
	}
}
