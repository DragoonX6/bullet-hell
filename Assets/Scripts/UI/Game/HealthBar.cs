﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class HealthBar: MonoBehaviour
{
	private Slider healthBar = null;

	public PlayerEntity player = null;

	private void Awake()
	{
		healthBar = GetComponent<Slider>();

		if(player == null)
			throw new MissingReferenceException("Missing player reference");
	}

	private void Update()
	{
		healthBar.value = (float)player.Health / player.maxHealth;
	}
}
