﻿using UnityEngine;
using InControl;
using System.Collections;

[RequireComponent(typeof(InputComponent))]
public class DeathScreenController: MonoBehaviour
{
	private InputComponent input = null;
	private DeathScreenControlSet pcs;

	[SerializeField]
	private ButtonController buttonController = null;

	[SerializeField]
	private ScorePanelController scorePanelController = null;

	public float moveTreshold = 0.3f;

	public float inputBlockTreshold = 0.3f;
	private float inputBlockTimer = 0;

	private bool blockInput = true;

	private bool firstFrame = true;

	private void Awake()
	{
		input = GetComponent<InputComponent>();
		input.enabled = false;

		if(buttonController == null)
			throw new MissingReferenceException("Missing button controller");

		if(scorePanelController == null)
			throw new MissingReferenceException("Missing Score panel controller");

		pcs = new DeathScreenControlSet();

		input.BindAction(pcs.confirmButton, InputComponent.State.pressed,
			ActionBinder.Bind(this, x => x.OnConfirmButtonPressed()));
		input.BindAxis(pcs.leftStick,
			ActionBinder.Bind<DeathScreenController, Vector2>(this,
			(x, value) => x.OnLeftStick(value)));
	}

	private void OnEnable()
	{
		if(firstFrame)
			return;

		if(!input.enabled)
			input.enabled = true;

		if(!scorePanelController.gameObject.activeSelf)
			scorePanelController.gameObject.SetActive(true);

		blockInput = true;
		inputBlockTimer = 0;
	}

	private IEnumerator Start()
	{
		firstFrame = false;

		yield return new WaitForEndOfFrame();

		OnEnable();
	}

	private void OnLeftStick(Vector2 value)
	{
		if(value.x >= moveTreshold && !blockInput)
		{
			buttonController.SelectPreviousButton();
		}
		else if(value.x <= -moveTreshold && !blockInput)
		{
			buttonController.SelectNextButton();
		}
		else if(blockInput && (value.x <= moveTreshold && value.x >= -moveTreshold))
		{
			blockInput = false;
		}
		else
		{
			buttonController.ResetTimer();
		}
	}

	private void OnConfirmButtonPressed()
	{
		buttonController.GetSelectedButton().OnPress();
	}

	private void Update()
	{
		if(inputBlockTimer < inputBlockTreshold && blockInput)
			inputBlockTimer += Time.deltaTime;
		else
		{
			blockInput = false;
		}
	}
}

class DeathScreenControlSet: PlayerActionSet
{
	private PlayerAction LSLeft;
	private PlayerAction LSRight;
	private PlayerAction LSUp;
	private PlayerAction LSDown;
	public PlayerTwoAxisAction leftStick;

	public PlayerAction confirmButton;

	public DeathScreenControlSet()
	{
		LSLeft = CreatePlayerAction("Death_LeftStick_Left");
		LSLeft.AddDefaultBinding(InputControlType.LeftStickLeft);
		LSRight = CreatePlayerAction("Death_LeftStick_Right");
		LSRight.AddDefaultBinding(InputControlType.LeftStickRight);
		LSUp = CreatePlayerAction("Death_LeftStick_Up");
		LSUp.AddDefaultBinding(InputControlType.LeftStickUp);
		LSDown = CreatePlayerAction("Death_LeftStick_Down");
		LSDown.AddDefaultBinding(InputControlType.LeftStickDown);

		leftStick = CreateTwoAxisPlayerAction(LSLeft, LSRight, LSDown, LSUp);

		confirmButton = CreatePlayerAction("Death_Confirm");
		confirmButton.AddDefaultBinding(InputControlType.Action1);
	}
}
