﻿using UnityEngine;

public class ScorePanelController: MonoBehaviour
{
	[SerializeField]
	private GameObject highScorePanel = null;

	[SerializeField]
	private GameObject scorePanel = null;

	private void OnEnable()
	{
		if(Game.Instance.HighScoreManager.placeTaken != -1)
		{
			highScorePanel.SetActive(true);
		}
		else
		{
			scorePanel.SetActive(true);
		}
	}

	private void OnDisable()
	{
		highScorePanel.SetActive(false);
		scorePanel.SetActive(false);
	}
}
