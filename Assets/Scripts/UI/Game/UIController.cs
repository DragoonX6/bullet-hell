﻿using UnityEngine;

public class UIController : MonoBehaviour
{
	[SerializeField]
	private GameObject pauseMenu = null;

	[SerializeField]
	private GameObject deathScreen = null;

	[SerializeField]
	private PlayerEntity player = null;

	private void Awake()
	{
		if(pauseMenu == null)
			throw new MissingReferenceException("Pause Menu has not been assigned");

		if(deathScreen == null)
			throw new MissingReferenceException("Death Screen has not been assigned");

		if(player == null)
			throw new MissingReferenceException("Player component has not been assigned");

		pauseMenu.SetActive(false);
		deathScreen.SetActive(false);
	}

	private void Update()
	{
		if(Game.Instance.paused && player.Health > 0)
		{
			if(!pauseMenu.activeSelf)
			{
				pauseMenu.SetActive(true);
				pauseMenu.GetComponent<PauseMenuController>().enabled = true;
			}
		}
		else if(!Game.Instance.paused && player.Health > 0)
		{
			if(pauseMenu.activeSelf)
				pauseMenu.SetActive(false);
		}
		else if(Game.Instance.paused && player.Health == 0)
		{
			if(!deathScreen.activeSelf)
			{
				deathScreen.SetActive(true);
				deathScreen.GetComponent<DeathScreenController>().enabled = true;
			}
		}
	}
}
