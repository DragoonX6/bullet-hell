﻿using UnityEngine;
using InControl;
using System.Collections;

[RequireComponent(typeof(InputComponent))]
public class PauseMenuController: MonoBehaviour
{
	private InputComponent input = null;
	private PauseControlSet pcs;

	[SerializeField]
	private ButtonController buttonController = null;

	public float moveTreshold = 0.3f;

	public float inputBlockTreshold = 0.3f;
	private float inputBlockTimer = 0;

	private bool blockInput = true;

	private bool firstFrame = true;

	private void Awake ()
	{
		input = GetComponent<InputComponent>();
		input.enabled = false;

		if(buttonController == null)
			throw new MissingReferenceException("Missing button controller");

		pcs = new PauseControlSet();

		input.BindAction(pcs.confirmButton, InputComponent.State.pressed,
			ActionBinder.Bind(this, x => x.OnConfirmButtonPressed()));
		input.BindAction(pcs.startButton, InputComponent.State.pressed,
			ActionBinder.Bind(this, x => x.OnBackButtonPressed()));
		input.BindAxis(pcs.leftStick, ActionBinder.Bind<PauseMenuController, Vector2>(this,
			(x, value) => x.OnLeftStick(value)));
	}

	private void OnEnable()
	{
		if(firstFrame)
			return;

		if(input.enabled == false)
			input.enabled = true;

		blockInput = true;
		inputBlockTimer = 0;
	}

	private IEnumerator Start()
	{
		firstFrame = false;

		yield return new WaitForEndOfFrame();

		input.enabled = true;
	}

	private void OnLeftStick(Vector2 value)
	{
		if(value.y >= moveTreshold && !blockInput)
		{
			buttonController.SelectPreviousButton();
		}
		else if(value.y <= -moveTreshold && !blockInput)
		{
			buttonController.SelectNextButton();
		}
		else if(blockInput && (value.y <= moveTreshold && value.y >= -moveTreshold))
		{
			blockInput = false;
		}
		else
		{
			buttonController.ResetTimer();
		}
	}

	public void OnBackButtonPressed()
	{
		Game.Instance.paused = false;
		input.enabled = false;
		buttonController.ResetSelection();
	}

	private void OnConfirmButtonPressed()
	{
		buttonController.GetSelectedButton().OnPress();
	}

	private void Update()
	{
		if(inputBlockTimer < inputBlockTreshold && blockInput)
			inputBlockTimer += Time.deltaTime;
		else
		{
			blockInput = false;
			enabled = false;
		}
	}
}

class PauseControlSet: PlayerActionSet
{
	private PlayerAction LSLeft;
	private PlayerAction LSRight;
	private PlayerAction LSUp;
	private PlayerAction LSDown;
	public PlayerTwoAxisAction leftStick;

	public PlayerAction confirmButton;
	public PlayerAction startButton;

	public PauseControlSet()
	{
		LSLeft = CreatePlayerAction("Pause_LeftStick_Left");
		LSLeft.AddDefaultBinding(InputControlType.LeftStickLeft);
		LSRight = CreatePlayerAction("Pause_LeftStick_Right");
		LSRight.AddDefaultBinding(InputControlType.LeftStickRight);
		LSUp = CreatePlayerAction("Pause_LeftStick_Up");
		LSUp.AddDefaultBinding(InputControlType.LeftStickUp);
		LSDown = CreatePlayerAction("Pause_LeftStick_Down");
		LSDown.AddDefaultBinding(InputControlType.LeftStickDown);

		leftStick = CreateTwoAxisPlayerAction(LSLeft, LSRight, LSDown, LSUp);

		confirmButton = CreatePlayerAction("Pause_Confirm");
		confirmButton.AddDefaultBinding(InputControlType.Action1);

		startButton = CreatePlayerAction("Pause_Start");
		startButton.AddDefaultBinding(InputControlType.Start);
		startButton.AddDefaultBinding(InputControlType.Action2);
	}
}
