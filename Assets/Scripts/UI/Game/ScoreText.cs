﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ScoreText: MonoBehaviour
{
	private Text scoreText = null;

	[SerializeField]
	private PlayerEntity player = null;

	[SerializeField]
	private bool useHighScore = false;

	[SerializeField]
	private string formatString = null;

	private int oldScore = 0;

	private void Awake()
	{
		scoreText = GetComponent<Text>();

		if(!useHighScore)
		{
			if(player == null)
				throw new MissingReferenceException("Missing player component");
		}
		else
		{
			scoreText.text = string.Format(formatString,
				Game.Instance.HighScoreManager.lastSubmittedHighScore);
		}
	}

	private void Update()
	{
		if(useHighScore)
			return;

		if(player.score != oldScore)
		{
			scoreText.text = string.Format(formatString, player.score);
			oldScore = player.score;
		}
	}
}
