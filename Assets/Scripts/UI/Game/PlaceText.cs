﻿using UnityEngine;
using UnityEngine.UI;

public class PlaceText: MonoBehaviour
{
	[SerializeField]
	private Text placeText = null;

	[SerializeField]
	private string formatString = null;

	private void Awake()
	{
		if(placeText == null)
			throw new MissingReferenceException("Missing text component");

		string placePostfix;

		int placeTaken = Game.Instance.HighScoreManager.placeTaken;

		switch(placeTaken)
		{
			case 1:
			{
				placePostfix = "st";
			}break;
			case 2:
			{
				placePostfix = "nd";
			}break;
			case 3:
			{
				placePostfix = "rd";
			}break;
			default:
            {
				placePostfix = "th";
			}break;
		}

		placeText.text = string.Format(formatString, placeTaken, placePostfix);
	}
}
