﻿#if !UNITY_EDITOR
using UnityEngine;
#endif

public class ExitButton: Button
{
	public override void OnPress()
	{
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
	}
}
