﻿using UnityEngine;

public class ObjectStateChangeButton: Button
{
	[SerializeField]
	private GameObject objectToChange = null;

	[SerializeField]
	private bool activate = true;

	private void Awake()
	{
		if(objectToChange == null)
			throw new MissingReferenceException("Missing object to change state for");
	}

	public override void OnPress()
	{
		objectToChange.SetActive(activate);
	}
}
