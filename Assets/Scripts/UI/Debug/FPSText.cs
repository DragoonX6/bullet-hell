﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class FPSText : MonoBehaviour
{
	private Text textComponent = null;

	private int frameCount = 0;
	private float deltaTime = 0.0f;
	private float fps = 0.0f;
	public float updateRate = 4.0f;

	private void Awake()
	{
		textComponent = GetComponent<Text>();
	}

	private void Update()
	{
		++frameCount;

		deltaTime += Time.deltaTime;

		if(deltaTime > 1.0f / updateRate)
		{
			fps = frameCount / deltaTime;
			frameCount = 0;
			deltaTime -= 1.0f / updateRate;

			textComponent.text = string.Format("FPS: {0}", fps);
		}
	}
}
