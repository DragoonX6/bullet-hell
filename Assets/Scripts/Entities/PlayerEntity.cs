﻿using System;
using System.Collections.Generic;

public class PlayerEntity: LivingEntity
{
	public List<Weapon> weapons = new List<Weapon>();

	public Weapon currentWeapon = null;

	private FloorController floorController;
	private EntityManager entityManager;

	public int score = 0;

	public bool isInvincible = false;

	protected override void Awake()
	{
		base.Awake();

		if(!currentWeapon)
			currentWeapon = weapons[0];

		if(!currentWeapon)
			throw new NullReferenceException("Must have at least one weapon");

		for(int i = 0; i < weapons.Count; ++i)
		{
			weapons[i].SetOwner(this);
		}

		floorController = Game.Instance.FloorController;
		entityManager = Game.Instance.EntityManager;

		OnWillTakeDamageEvent += OnWillTakeDamage;
		OnDamageTakenEvent += DefaultOnDamageTaken;

		if(isInvincible)
			OnWillDieEvent += Invincible;
		else
			OnWillDieEvent += DefaultOnWillDie;

		OnDeathEvent += OnDeath;
    }

	private void Start()
	{
		floorController.InitBoundaries(gameObject);
	}

	public int OnWillTakeDamage(Entity sender, Entity projectile, int damage)
	{
		Game.Instance.SpawnPool.PutBack(projectile);
		return damage;
	}

	public void OnDeath(Entity Killer)
	{
		HighScoreManager highScore = Game.Instance.HighScoreManager;
		highScore.SubmitScore(score);

		Game.Instance.paused = true;
	}

	public bool Invincible()
	{
		return false;
	}

	private void FixedUpdate()
	{
		List<FloorTile> tiles = floorController.GetTiles(transform.position);
		tiles.ForEach(x => entityManager.EntitySendMessage(this, x,
			EntityManager.MessageType.genericMessage, null));
	}

	private void Update()
	{
		if(Game.Instance.paused)
			return;

		AnnounceLocationArguments args = new AnnounceLocationArguments
		{
			location = transform.position
		};

		entityManager.EntitySendMessageToAllOfType(this, EntityType.buoy,
			EntityManager.MessageType.announceLocation, args);

		entityManager.EntitySendMessageToAllOfType(this, EntityType.genericEnemy,
			EntityManager.MessageType.announceLocation, args);
	}

	public override EntityType Type()
	{
		return EntityType.player;
	}

	public override void OnEntityReceiveMessage(Entity sender,
		EntityManager.MessageType messageType, EventArgs arguments)
	{
		base.OnEntityReceiveMessage(sender, messageType, arguments);

		switch(messageType)
		{
			case EntityManager.MessageType.score:
			{
				ScoreArguments scoreArgs = arguments as ScoreArguments;
				score += scoreArgs.score;
			}break;
		}
	}

	public void OnRightTrigger()
	{
		currentWeapon.Fire();
	}
}
