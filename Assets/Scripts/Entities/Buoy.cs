﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class Buoy: LivingEntity
{
	public Weapon weapon = null;
	private FloorController floorController;
	private EntityManager entityManager;

	private float playerDistance = 0;
	private Camera renderCamera = null;

	private static Buoy cachedPrefab;

	[RuntimeInitializeOnLoadMethod]
	static void RegisterFactory()
	{
		EntityManager.RegisterFactory(EntityType.buoy, Factory);
	}

	public static Buoy Factory()
	{
		if(!cachedPrefab)
			cachedPrefab = Resources.Load<Buoy>("Buoy");

		return Instantiate(cachedPrefab);
	}

	protected override void Awake ()
	{
		base.Awake();

		if(!weapon)
			throw new NullReferenceException("Must have at least one weapon");

		weapon.SetOwner(this);

		floorController = Game.Instance.FloorController;
		entityManager = Game.Instance.EntityManager;

		renderCamera = Game.Instance.RenderCamera;

		OnWillTakeDamageEvent += DefaultOnWillTakeDamage;
		OnDamageTakenEvent += DefaultOnDamageTaken;
		OnWillDieEvent += DefaultOnWillDie;
		OnDeathEvent += DefaultOnDeath;
	}

	public override void OnEntityReceiveMessage(Entity sender,
		EntityManager.MessageType messageType, EventArgs arguments)
	{
		base.OnEntityReceiveMessage(sender, messageType, arguments);

		switch(messageType)
		{
			case EntityManager.MessageType.announceLocation:
			{
				AnnounceLocationArguments arg = (AnnounceLocationArguments)arguments;
				playerDistance = Vector3.Distance(transform.position, arg.location);
			}break;
		}
	}

	public override EntityType Type()
	{
		return EntityType.buoy;
	}

	private void Update()
	{
		if(Game.Instance.paused)
			return;

		if(playerDistance < renderCamera.farClipPlane + 15)
			weapon.Fire();

		if(playerDistance > (renderCamera.farClipPlane * 2))
			DefaultOnDeath(null);
	}

	private void FixedUpdate()
	{
		List<FloorTile> tiles = floorController.GetTiles(transform.position);
		for(int i = 0; i < tiles.Count; ++i)
		{
			entityManager.EntitySendMessage(this, tiles[i],
				EntityManager.MessageType.genericMessage, null);
		}
	}
}
