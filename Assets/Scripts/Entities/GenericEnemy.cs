﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class GenericEnemy : LivingEntity
{
	private FloorController floorController;
	private EntityManager entityManager;

	public Weapon[] potentialWeapons = null;
	private Weapon currentWeapon = null;

	[Tooltip("The maximum distance from the player before we start attacking.")]
	public float maxDistance = 0;
	[Tooltip("The minimum distance from the player before we start attacking.")]
	public float minDistance = 0;
	private float distanceChosen = 0;

	private Vector3 target = Vector3.zero;

	private bool rotateRight = true;

	private static GenericEnemy cachedPrefab;
	private static XXHash randomEngine = null;
	private static int engineCycles = 0;

	private Camera renderCamera = null;

	[RuntimeInitializeOnLoadMethod]
	static void RegisterFactory()
	{
		EntityManager.RegisterFactory(EntityType.genericEnemy, Factory);
	}

	public static GenericEnemy Factory()
	{
		if(!cachedPrefab)
			cachedPrefab = Resources.Load<GenericEnemy>("GenericEnemy");

		return Instantiate(cachedPrefab);
	}

	public override EntityType Type()
	{
		return EntityType.genericEnemy;
	}

	protected override void Awake()
	{
		base.Awake();

		if(potentialWeapons == null || potentialWeapons[0] == null)
			throw new MissingReferenceException("Need at least one weapon");

		if(randomEngine == null)
			randomEngine = new XXHash(Time.frameCount);

		distanceChosen = randomEngine.Range(minDistance, maxDistance, engineCycles++);

		currentWeapon = potentialWeapons[randomEngine.Range(0,
			potentialWeapons.Length, engineCycles++)];
		currentWeapon.gameObject.SetActive(true);
		currentWeapon.SetOwner(this);

		floorController = Game.Instance.FloorController;
		entityManager = Game.Instance.EntityManager;

		OnWillTakeDamageEvent += DefaultOnWillTakeDamage;
		OnDamageTakenEvent += DefaultOnDamageTaken;
		OnWillDieEvent += DefaultOnWillDie;
		OnDeathEvent += DefaultOnDeath;

		renderCamera = Game.Instance.RenderCamera;
	}

	public override void OnEntityReceiveMessage(Entity sender,
		EntityManager.MessageType messageType, EventArgs arguments)
	{
		base.OnEntityReceiveMessage(sender, messageType, arguments);

		switch(messageType)
		{
			case EntityManager.MessageType.announceLocation:
			{
				AnnounceLocationArguments locationArgs =
					(AnnounceLocationArguments)arguments;

				target = locationArgs.location;
			}break;
		}
	}

	private void Update()
	{
		if(Game.Instance.paused)
			return;

		target.y = transform.position.y;
		Vector3 direction = target - transform.position;

		float distance = direction.magnitude;
		Vector3 normalizedDirection = direction.normalized;

		Vector3 screenPos = renderCamera.WorldToViewportPoint(transform.position);

		float currentMovementSpeed = movementSpeed;

		if(screenPos.x < 0 || screenPos.x > 1 || screenPos.y < 0 || screenPos.y > 1)
			currentMovementSpeed *= 2;

		if(distance > distanceChosen + 0.05f)
			transform.position = transform.position + normalizedDirection *
				currentMovementSpeed * Time.deltaTime;
		else
		{
			float currentAngle = Mathf.Atan2(-direction.z, -direction.x) * Mathf.Rad2Deg;

			currentAngle += 90f;
			currentAngle = ((currentAngle) + 180f) % 360f;
			currentAngle = 360f - currentAngle;

			if(rotateRight)
			{
				if(currentAngle > 40f && currentAngle < 180f)
				{
					rotateRight = false;
				}
			}
			else
			{
				if(currentAngle < 320f && currentAngle > 180f)
				{
					rotateRight = true;
				}
			}

			bool willShoot = true;

			if((currentAngle > 40f && currentAngle < 180f) ||
				(currentAngle < 320f && currentAngle > 180f))
            {
				willShoot = false;
				currentMovementSpeed *= 2;
			}

			currentAngle += rotateRight ? 5f : -5f;

			Vector3 targetPosition = GetPointOnCircle(target, distanceChosen,
				currentAngle);
			targetPosition.y = transform.position.y;

			Vector3 targetDirection = (targetPosition - transform.position).normalized;

			transform.position = transform.position + targetDirection *
				currentMovementSpeed * Time.deltaTime;

			transform.LookAt(target);

			if(willShoot)
				currentWeapon.Fire();
		}

		transform.LookAt(target);
	}

	private void FixedUpdate()
	{
		List<FloorTile> tiles = floorController.GetTiles(transform.position);
		tiles.ForEach(x => entityManager.EntitySendMessage(this, x,
			EntityManager.MessageType.genericMessage, null));
	}

	private Vector3 GetPointOnCircle(Vector3 origin, float radius, float angle)
	{
		angle *= Mathf.Deg2Rad;

		float z = origin.z + radius * Mathf.Cos(angle);
		float x = origin.x + radius * Mathf.Sin(angle);

		return new Vector3(x, origin.y, z);
	}
}
