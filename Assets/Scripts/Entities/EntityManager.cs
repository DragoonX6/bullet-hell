﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class EntityManager: MonoBehaviour
{
	private List<Entity> entities;

	private static Dictionary<Entity.EntityType, Func<Entity>> factories;

	public enum MessageType
	{
		none,
		genericMessage,
		damage,
		announceLocation,
		score
	}

	static EntityManager()
	{
		factories = new Dictionary<Entity.EntityType, Func<Entity>>();
	}

	private EntityManager()
	{
		entities = new List<Entity>();
	}

	public void EntitySendMessage(Entity sender,
		Entity receiver, MessageType messageType, EventArgs arguments)
	{
		receiver.OnEntityReceiveMessage(sender, messageType, arguments);
    }

	public void EntitySendMessageToAllOfType(Entity sender,
		Entity.EntityType type, MessageType messageType, EventArgs arguments)
	{
		for(int i = 0; i < entities.Count; ++i)
		{
			if(entities[i].Type() == type)
			{
				entities[i].OnEntityReceiveMessage(sender, messageType, arguments);
			}
		}
	}

	public void RegisterEntity(Entity entity)
	{
		entities.Add(entity);
	}

	public static void RegisterFactory(Entity.EntityType entityType, Func<Entity> factory)
	{
		if(!factories.ContainsKey(entityType))
		{
			factories.Add(entityType, factory);
		}
	}

	public static Entity Factory(Entity.EntityType typeID)
	{
		if(factories.ContainsKey(typeID))
		{
			return factories[typeID]();
		}

		return null;
	}
}

public class DamageArguments: EventArgs
{
	public Entity sender;
	public int damage;
}

public class AnnounceLocationArguments: EventArgs
{
	public Vector3 location;
}

public class ScoreArguments: EventArgs
{
	public int score;
}
