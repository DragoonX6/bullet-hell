﻿using System;
using UnityEngine;

public abstract class LivingEntity : Entity
{
	#region delegates
	public Func<Entity, Entity, int, int> OnWillTakeDamageEvent;
	public Action<Entity, Entity, int> OnDamageTakenEvent;
	public Func<bool> OnWillDieEvent;
	public Action<Entity> OnDeathEvent;
	#endregion
	public int maxHealth = 100;

	public float movementSpeed = 10.0f;

	public int giveScore = 0;

	private bool isDead = false;

	private int health;
	public int Health
	{
		get
		{
			return health;
		}
		set
		{
			health = Mathf.Clamp(value, 0, maxHealth);
		}
	}

	protected override void Awake ()
	{
		base.Awake();

		health = maxHealth;
	}

	private void OnEnable()
	{
		isDead = false;
	}

	public override void OnEntityReceiveMessage(Entity sender,
		EntityManager.MessageType messageType, EventArgs arguments)
	{
		switch(messageType)
		{
			case EntityManager.MessageType.damage:
			{
				DamageArguments dmgArgs = arguments as DamageArguments;

				int takenDamage = OnWillTakeDamageEvent(dmgArgs.sender, sender, dmgArgs.damage);
				OnDamageTakenEvent(dmgArgs.sender, sender, takenDamage);

				if(Health == 0)
				{
					bool willDie = OnWillDieEvent();
					if(willDie)
					{
						OnDeathEvent(dmgArgs.sender);
						isDead = true;
					}
				}
			}break;
		}
	}

	public int DefaultOnWillTakeDamage(Entity sender, Entity projectile, int damage)
	{
		return damage;
	}

	public void DefaultOnDamageTaken(Entity sender, Entity projectile, int damage)
	{
		Health -= damage;

		Game.Instance.SpawnPool.PutBack(projectile);
	}

	public bool DefaultOnWillDie()
	{
		return !isDead;
	}

	public void DefaultOnDeath(Entity killer)
	{
		if(killer != null)
		{
			ScoreArguments scoreArgs = new ScoreArguments() { score = giveScore };
			Game.Instance.EntityManager.EntitySendMessage(this, killer,
				EntityManager.MessageType.score, scoreArgs);
		}

		Game.Instance.SpawnPool.PutBack(this);
		--Game.Instance.EnemySpawner.enemyCount;
	}
}
