﻿using System;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Projectile: Entity
{
	public int damage = 0;
	public float speed = 50f;
	public float maxDistanceTraveled = 100f;
	private float distanceTraveled = 0.0f;

	[HideInInspector]
	public Entity owningEntity = null;

	[HideInInspector]
	public Vector3 movementDirection = Vector3.forward;

	[HideInInspector]
	public int collisionLayer = 0;

	private static Projectile cachedPrefab = null;

	private SpawnPool spawnPool = null;
	private EntityManager entityManager = null;

	[RuntimeInitializeOnLoadMethod]
	static void RegisterFactory()
	{
		EntityManager.RegisterFactory(EntityType.projectile, Factory);
	}

	public static Projectile Factory()
	{
		if(!cachedPrefab)
			cachedPrefab = Resources.Load<Projectile>("Projectile");

        return Instantiate(cachedPrefab);
	}

	private void OnEnable()
	{
		distanceTraveled = 0.0f;
	}

	protected override void Awake()
	{
		base.Awake();

		Collider collider = GetComponent<Collider>();
		collider.isTrigger = true;

		spawnPool = Game.Instance.SpawnPool;
		entityManager = Game.Instance.EntityManager;
	}

	private void Update()
	{
		if(Game.Instance.paused)
			return;

		float travelDistance = speed * Time.deltaTime;

		RaycastHit hitInfo;
		if(Physics.Raycast(transform.position, movementDirection, out hitInfo,
			travelDistance, collisionLayer))
		{
			transform.position += movementDirection * hitInfo.distance;
		}
		else
			transform.position += movementDirection * travelDistance;

		if(!owningEntity || !owningEntity.spawned)
		{
			distanceTraveled += travelDistance;

			if(distanceTraveled >= maxDistanceTraveled)
				spawnPool.PutBack(this);
		}
		else
		{
			if(Vector3.Distance(transform.position, owningEntity.transform.position) >=
				maxDistanceTraveled)
				spawnPool.PutBack(this);
		}
	}

	public override EntityType Type()
	{
		return EntityType.projectile;
	}

	public override void OnEntityReceiveMessage(Entity sender,
		EntityManager.MessageType messageType, EventArgs arguments)
	{
		if(messageType == EntityManager.MessageType.damage)
		{
			spawnPool.PutBack(this);
		}
	}

	public void OnTriggerEnter(Collider other)
	{
		Entity entity = other.GetComponent<Entity>();
		if(entity)
		{
			entityManager.EntitySendMessage(this, entity,
				EntityManager.MessageType.damage, new DamageArguments(){
					sender = owningEntity, damage = damage });
		}
	}
}
