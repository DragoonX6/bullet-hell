﻿using UnityEngine;
using System;

public abstract class Entity: MonoBehaviour
{
	public enum EntityType
	{
		none,
		floorTile,
		player,
		projectile,
		buoy,
		genericEnemy
	}

	public bool spawned = true;

	protected virtual void Awake()
	{
		Game.Instance.EntityManager.RegisterEntity(this);
    }

	public abstract void OnEntityReceiveMessage(Entity sender,
		EntityManager.MessageType messageType, EventArgs arguments);

	public abstract EntityType Type();
}
