﻿using UnityEngine;
using System;
using System.Collections.ObjectModel;

public class HighScoreManager: MonoBehaviour
{
	private int[] scores = new int[10];

	public int lastSubmittedHighScore = -1;
	public int placeTaken = -1;

	private void Awake()
	{
		for(int i = 0; i < scores.Length; ++i)
		{
			scores[i] = PlayerPrefs.GetInt(string.Format("Score{0}", i), 0);
		}

		Array.Sort(scores, (x, y) =>
		{
			return x < y ? 1 : x > y ? -1 : 0;
		});
	}

	public ReadOnlyCollection<int> GetHighScores()
	{
		return Array.AsReadOnly(scores);
	}

	public void SubmitScore(int score)
	{
		int highest = -1, index = -1;

		for(int i = 0; i < scores.Length; ++i)
		{
			if(score >= scores[i] && !(scores[i] <= highest))
			{
				index = i;
				highest = scores[i];
			}
		}

		if(index == -1)
		{
			lastSubmittedHighScore = score;
			placeTaken = -1;
			return;
		}

		int[] newScores = new int[scores.Length - index];
		newScores[0] = score;

		for(int i = index, j = 1; j < newScores.Length; ++i, ++j)
		{
			newScores[j] = scores[i];
		}

		for(int i = index, j = 0; i < scores.Length; ++i, ++j)
		{
			scores[i] = newScores[j];
		}

		lastSubmittedHighScore = score;
		placeTaken = index + 1;

		SaveHighScore();
	}

	private void SaveHighScore()
	{
		Array.Sort(scores, (x, y) =>
		{
			return x < y ? 1 : x > y ? -1 : 0;
		});

		for(int i = 0; i < scores.Length; ++i)
		{
			PlayerPrefs.SetInt(string.Format("Score{0}", i), scores[i]);
		}

		PlayerPrefs.Save();
	}
}
